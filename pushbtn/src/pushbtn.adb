with Ada.Text_Io; use Ada.Text_Io;

with realtime ;
with rpibutton ;
with raspio.GPIO ;

procedure Pushbtn is
   btn : realtime.Button_Ptr_Type ;
   btnstate : Boolean ;
begin
   btn := rpibutton.Create(raspio.GPIO.GPIO_P1_11);
  loop
      delay 5.0 ;
      btn.Last( btnstate );
      Put("State of the button is "); Put( btnstate'Image); New_Line ;
   end loop ;
end Pushbtn;
