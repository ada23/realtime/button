with Ada.Text_Io; use Ada.Text_Io;
with Ada.Integer_Text_Io; use Ada.Integer_Text_Io;

package body rpibutton is
    use raspio.GPIO ;
    pins : array (1..4) of raspio.GPIO.Pin_Type;

    function Create( gpio : raspio.GPIO.Pin_Id_Type ) return realtime.Button_Ptr_Type is
       result : realtime.Button_Ptr_Type ;
    begin
        pins(1) := Raspio.GPIO.Create
                                        (Pin_ID => gpio , 
                                        Mode => Input ,
                                        Internal_Resistor => Pull_Up );
        result := new realtime.Button_Type;
        result.Monitor( "rpibtn" , 0.1 , 1 , gpio_acquire'Access , gpio_changed_callback'Access);
        return result ;
    end Create ;

    function gpio_acquire( h : Integer ) return Boolean is
    begin
        if raspio.gpio.Read (pins(1)) = raspio.GPIO.On 
        then
            return true ;
        end if ;
        return false ;
    end gpio_acquire ;

    procedure gpio_changed_callback( h : Integer ; newstate : boolean ) is
    begin
        Put("Button "); Put(h); Put(" NewState "); Put(newstate'Image); New_Line ;
    end gpio_changed_callback ;
begin
   Raspio.Initialize;
end rpibutton ;
