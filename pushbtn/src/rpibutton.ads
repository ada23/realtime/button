with realtime ;
with raspio.gpio ;
package rpibutton is

    function Create( gpio : raspio.GPIO.Pin_Id_Type ) return realtime.Button_Ptr_Type ;

    function gpio_acquire( h : Integer ) return Boolean ;
    procedure gpio_changed_callback( h : Integer ; newstate : boolean );

end rpibutton ;
